**How to use:**

1. Download and install the plugins from /assets/additional plugins.txt
2. The main scene is ./assets/allmainscenes/MetaScene.unity
3. The experience logic resides in the GameObject "Director" using the DirectorScript.cs. 
4. Because we want a smooth experience without fade ins/outs all the scenes reside in one Unity level. 
5. There is only one VRoomElevator containing the OVRCameraController that will be "teleported" from scene to scene
6. The VRoomElevator contains a GameObject called "MovieSurface" which will serve as the TV screen for the movie clips. It uses the script "GearVRMoviePlayer.cs" which has very basic media playback capabilities. The "DirectorScript.cs" controls the movie player via a few public methods: LoadFile(string filename), Play(), Pause(), Stop() and GetCurrentMovieLength().