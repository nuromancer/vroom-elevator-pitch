/*------------------------------------------------
// CUBEMAPPER
// Version: See Readme
// Created by: Rainer Liessem
// Website: http://www.spreadcamp.com
//
// PLEASE RESPECT THE LICENSE TERMS THAT YOU
// AGREED UPON WITH YOUR PURCHASE OF THIS ASSET
------------------------------------------------*/
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Cubemapper
{
    public class CubemapUser : MonoBehaviour
    {
        public bool startupSwap = false;
        public bool realtimeSwitching = false; // this user will always use the nearest node to fetch a cubemap
        public enum BlendMode { Distance = 0, Radius = 1 }
        public BlendMode blendMode = BlendMode.Distance;

        private CubemapNode[] nodes = null; // stores a reference to all the nodes in the scene	
        private List<Material> compatibleMaterials = new List<Material>();
        private Transform trans;

        // Blending Variables
        private CubemapNode _currentNode = null;
        private CubemapNode _nearestNode = null;
        private bool softBlending = false; // set to true automatically if material with _Cube2 property found
        private float fadeTime = 1f; // speed of soft blending transitions
        private Cubemap _curCubemap;
        private Cubemap _targetCubemap;

        // Radius Blending Variables
        private List<CubemapNode> _enteredNodes = new List<CubemapNode>(); // used only by radius blending
        private CubemapNode blendNode1;
        private CubemapNode blendNode2;
        
        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;

            if (_nearestNode != null)
                Gizmos.DrawLine(transform.position, _nearestNode.transform.position);

            if (blendNode1 != null && blendNode2 != null)
            {
                Gizmos.DrawLine(transform.position, blendNode1.transform.position);
                Gizmos.DrawLine(transform.position, blendNode2.transform.position);
            }
        }

        void Start()
        {
            // Verify and abort if no Material on this object supports Cubemaps
            VerifyShaderCubemapSupport();

            // Store reference to all nodes in the scene, abort if none found
            nodes = GetAllNodes();
            if (nodes.Length == 0)
                Destroy(this.GetComponent<CubemapUser>());

            // Cache commonly used components
            trans = transform;

            // If required we will first assign the nearest node's cubemap and set it as current
            if (startupSwap || realtimeSwitching)
            {
                _currentNode = FindNearestNode();
                StartCoroutine(SwitchCubemapSimple(_currentNode));
            }

            // Start processing cubemaps via co-routine if we are supposed to do real-time switching
            if (realtimeSwitching)
            {
                StartCoroutine("WatchEnviroment");
            }
        }

        void OnDestroy()
        {
            // (Try to) prevent leaking of our instanced materials if the object gets destroyed
            foreach (Material mat in compatibleMaterials)
            {
                Destroy(mat);
            }
        }

        CubemapNode[] GetAllNodes()
        {
            CubemapNode[] nodes;
            nodes = FindObjectsOfType(typeof(CubemapNode)) as CubemapNode[];
            //Debug.Log(nodes.Length + " | " + nodes);
            return (nodes.Length == 0) ? null : nodes;
        }

        /// <summary>
        /// Observes some changes in order to initiate blending
        /// </summary>
        IEnumerator WatchEnviroment()
        {
            while (true)
            {
                // More expensive blending using radius defined by Nodes, only works if softBlending is true
                if (softBlending && blendMode == BlendMode.Radius)
                {
                    _enteredNodes = FindEnteredNodes();

                    // If just under influence of one node, fade to it using the simple way
                    if (_enteredNodes.Count == 1)
                    {
                        blendNode1 = null;
                        blendNode2 = null;

                        _nearestNode = _enteredNodes[0];
                        if (_nearestNode != _currentNode)
                        {
                            yield return StartCoroutine(SwitchCubemapSimple(_nearestNode));
                            _currentNode = _nearestNode;
                        }
                    }
                    // More than one Node, so we can proceed with the more expensive blend
                    else if (_enteredNodes.Count > 1)
                    {
                        // From now on we only work with the first two entries in the list
                        // since our shader is made for blending only two cubemaps
                        blendNode1 = _enteredNodes[0];
                        blendNode2 = _enteredNodes[1];

                        // TODO
                        // We actually want to do some more computations in the future with some decent math
                        // I started something but couldn't finish it satisfactorily, so for now I just check
                        // which of the two we're nearest to and go to that like with the simple blends.
                        // If you find a decent way to consider Intersections etc before I do,
                        // I'd love if you could share that information with me
                        //yield return StartCoroutine("SwitchCubemapByRadius"); // Blend by Distance within Radius
                        float dist1 = Vector3.Distance(trans.position, blendNode1.transform.position);
                        float dist2 = Vector3.Distance(trans.position, blendNode2.transform.position);
                        _nearestNode = (dist1 < dist2) ? blendNode1 : blendNode2;
                        if (_nearestNode != _currentNode)
                        {
                            yield return StartCoroutine(SwitchCubemapSimple(_nearestNode));
                            _currentNode = _nearestNode;
                        }
                    }
                }
                // Simple Proximity Blending
                else
                {
                    _nearestNode = FindNearestNode();

                    // Nearest Node differs from current node
                    if (_nearestNode != _currentNode)
                    {
                        yield return StartCoroutine(SwitchCubemapSimple(_nearestNode)); // Simple or Smooth Blend by Proximity
                        _currentNode = _nearestNode;
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }

        /*
        /// <summary>
        /// DONT USE THIS IS BUGGY (I don't get a proper range I can blend with between intersections in this version)
        /// More expensive switch to the Cubemap of a Node Target, using a radius on the target and our current position
        /// within a intersection to determine the blend factor. Requires softBlend = true + use of Replacement Shaders (_Cube2 property)
        /// </summary>
        /// <returns>Semi-accurate blending</returns>
        IEnumerator SwitchCubemapByRadius()
        {
            float curBlend = 0.0f;
            float dist1, dist2;
            float mag1, mag2;
            float direction;
            bool isFading = true;

            // Blending
            while (isFading)
            {
                // We can assume we are within the radius of 2+ nodes or else we wouldn't be executing this.
                // Now it is a matter of finding out the blend values to use...
                dist1 = Vector3.Distance(trans.position, blendNode1.transform.position);
                dist2 = Vector3.Distance(trans.position, blendNode2.transform.position);
                mag1 = dist1 / blendNode1.blendRadius;
                mag2 = dist2 / blendNode2.blendRadius;
                direction = mag1 - mag2;

                // Set Target Cubemap
                if (direction > 0) // left
                {
                    _curCubemap = blendNode1.cubemap;
                    _targetCubemap = blendNode2.cubemap;
                }
                else if (direction < 0) // right
                {
                    _curCubemap = blendNode2.cubemap;
                    _targetCubemap = blendNode1.cubemap;
                }

                SetCubemapToMaterials("_Cube", _curCubemap);
                SetCubemapToMaterials("_Cube2", _targetCubemap);

                // Are we within that nodes radius?
                if (direction > 0)
                {
                    curBlend = direction;
                    curBlend = Mathf.Clamp(curBlend, 0.5f, 1f);
                    SetBlendFloat(curBlend);

                    isFading = true;
                }
                else if (direction < 0)
                {
                    curBlend = -direction;
                    curBlend = Mathf.Clamp(curBlend, 0.5f, 1f);
                    SetBlendFloat(curBlend);

                    isFading = true;
                }

                // End loop if we are outside radius or achieved blending target
                if ((curBlend >= 1.0f || curBlend <= 0.0f) || (dist1 >= blendNode1.blendRadius || dist2 >= blendNode2.blendRadius))
                {
                    _curCubemap = _targetCubemap;
                    SetCubemapToMaterials("_Cube", _curCubemap);
                    SetCubemapToMaterials("_Cube2", null);
                    curBlend = 0.0f;
                    SetBlendFloat(curBlend);
                    isFading = false;
                }

                Debug.Log("Cur Blend: " + curBlend + " Dir " + direction);

                yield return null;
            }

            yield return null;
        }
        */

        /// <summary>
        /// Simple cubemap switch to the Cubemap of a Node Target
        /// </summary>
        /// <param name="targetNode">Target Node to fetch Cubemap from</param>
        /// <returns>Instant Switch or Lerp of Material Properties</returns>
        IEnumerator SwitchCubemapSimple(CubemapNode targetNode)
        {
            _targetCubemap = targetNode.cubemap;

            // Soft-blend cubemap over time
            if (softBlending)
            {
                bool isFading = true;
                float elapsedTime = 0.0f;
                float curBlend = 0.0f;

                SetBlendFloat(curBlend);
                SetCubemapToMaterials("_Cube2", _targetCubemap);

                while (isFading)
                {
                    elapsedTime += Time.deltaTime;

                    if (elapsedTime >= fadeTime || curBlend >= 1.0f)
                    {
                        curBlend = 0.0f;
                        _curCubemap = _targetCubemap;
                        SetCubemapToMaterials("_Cube", _curCubemap);
                        SetCubemapToMaterials("_Cube2", null);
                        isFading = false;
                    }
                    else
                    {
                        curBlend = Mathf.Lerp(curBlend, 1.0f, elapsedTime / fadeTime);
                    }

                    SetBlendFloat(curBlend);
                    //Debug.Log("CurBlend: " + curBlend + " | Time elapsed: " + elapsedTime);

                    yield return null;
                }
            }
            // Instant switching
            else
            {
                SetCubemapToMaterials("_Cube", _targetCubemap);
                //Debug.Log("Assigned Cubemap " + c.name + " to \"" + gameObject.name + "\" Material(s)!");
            }

            yield return null;
        }

        /// <summary>
        /// Find the nearest node relative to the Cubemap User
        /// </summary>
        /// <returns>Cubemap Node reference</returns>
        CubemapNode FindNearestNode()
        {
            CubemapNode cn = null;
            float minDist = Mathf.Infinity;
            Vector3 pos = trans.position;

            // Find nearest node
            for (int i = 0; i < nodes.Length; i++)
            {
                float dist = Vector3.Distance(nodes[i].transform.position, pos);

                if (dist < minDist && nodes[i].cubemap != null)
                {
                    cn = nodes[i];
                    minDist = dist;
                }
            }

            //Debug.Log("Nearest Node: " + cn.name + "(Distance: " + minDist + ")");
            return cn;
        }

        /// <summary>
        /// Find all nodes that we are within the radius of
        /// </summary>
        /// <returns>Cubemap Nodes</returns>
        List<CubemapNode> FindEnteredNodes()
        {
            List<CubemapNode> tmpNodes = new List<CubemapNode>();
            Vector3 pos = transform.position;

            // Find nearest node
            for (int i = 0; i < nodes.Length; i++)
            {
                float dist = Vector3.Distance(nodes[i].transform.position, pos);

                if (dist <= nodes[i].blendRadius && nodes[i].cubemap != null)
                {
                    tmpNodes.Add(nodes[i]);
                }
            }

            //foreach (CubemapNode node in tmpNodes) Debug.Log("Entered Nodes: " + node.name);
            return tmpNodes;
        }

        /// <summary>
        /// Verify that Cubemaps are applicable to at least one Material found on this game object
        /// </summary>
        void VerifyShaderCubemapSupport()
        {
            Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();

            bool hasCubemapSupport = false; // this will set true in the loop later eventually, if it doesn't we know there's no cubemap support anywhere on this object

            if (renderers != null)
            {
                // Iterate through all renderers
                foreach (Renderer r in renderers)
                {
                    foreach (Material mat in r.materials)
                    {
                        if (mat.HasProperty("_Cube"))
                        {
                            hasCubemapSupport = true;
                            compatibleMaterials.Add(mat);
                        }

                        if (mat.HasProperty("_Cube2"))
                            softBlending = true;
                    }
                }

                // If it turns out we don't have any material with cubemap support, we delete this component
                if (!hasCubemapSupport)
                {
                    Debug.LogError(gameObject.name.ToString() + " is a Cubemap User, but no Materials have a _Cube property for attaching Cubemaps!");
                    Destroy(this.GetComponent<CubemapUser>());
                }
            }
            else
            {
                Debug.LogError("No renderer found for object" + gameObject.name.ToString());
                Destroy(this.GetComponent<CubemapUser>());
            }
        }

        /// <summary>
        /// Goes through each material in our compatible array to assign the given Cubemap to a property
        /// </summary>
        /// <param name="property">Property of the Material Shader to affect</param>
        /// <param name="c">The Cubemap to set</param>
        void SetCubemapToMaterials(string property, Cubemap c)
        {
            for (int i = 0; i < compatibleMaterials.Count; i++)
            {
                if (compatibleMaterials[i].HasProperty(property))
                {
                    compatibleMaterials[i].SetTexture(property, c);
                    _curCubemap = c;
                    //Debug.Log(trans.name + " set cubemap " + c.name);
                }
            }
        }

        /// <summary>
        /// Set the Blend value on shader if it exists there. Ranges from 0 - 1
        /// </summary>
        /// <param name="value">Blend Value</param>
        void SetBlendFloat(float value)
        {
            for (int i = 0; i < compatibleMaterials.Count; i++)
            {
                if (compatibleMaterials[i].HasProperty("_Blend"))
                    compatibleMaterials[i].SetFloat("_Blend", value);
            }
        }
    }
}