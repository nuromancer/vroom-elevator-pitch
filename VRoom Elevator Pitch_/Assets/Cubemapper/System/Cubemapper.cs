﻿/*------------------------------------------------
// CUBEMAPPER
// Version: See Readme
// Created by: Rainer Liessem
// Website: http://www.spreadcamp.com
//
// PLEASE RESPECT THE LICENSE TERMS THAT YOU
// AGREED UPON WITH YOUR PURCHASE OF THIS ASSET
------------------------------------------------*/
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Cubemapper
{
    public class CMVersion
    {
        public static string assetURL = "https://www.assetstore.unity3d.com/#/content/3041";
        public static string currentVersion = "1.5";
        public static string latestVersion = "-";
        public static string checkResult;
        public static bool isOutdated = false;
                
        public static void CheckForUpdates()
        {
            int currentVersion_Primary; // primary version number (like the "1" in 1.5)
            int currentVersion_Sub;     // sub version number (like the "5" in 1.5)
            string[] curSplit = currentVersion.Split(new char[] { '.' });
            currentVersion_Primary = int.Parse(curSplit[0]);
            currentVersion_Sub = int.Parse(curSplit[1]);

            checkResult = "Checking for updates...";
            System.Net.WebClient webClient = new System.Net.WebClient();
            try
            {
                latestVersion = webClient.DownloadString("http://www.spreadcamp.com/docs/cubemapper/version.php");

                string[] split = latestVersion.Split(new char[] { '.' });
                int latest_Primary = int.Parse(split[0]);
                int latest_Sub = int.Parse(split[1]);

                // Primary version higher already? Then outdated
                if (latest_Primary > currentVersion_Primary)
                    isOutdated = true;
                else
                    // Finer check with the subversion
                    isOutdated = (latest_Sub > currentVersion_Sub) ? true : false;

                checkResult = (isOutdated) ? "Outdated! Please update." : "Up to date!";
            }
            catch (System.Net.WebException ex)
            {
                checkResult = "Couldn't check for updates: " + ex.Status;
            }
        }
    }

    public class CMFile : MonoBehaviour
    {
        /// <summary>
        /// Transform a given path into something that Unity can read. Requires presence of "Assets" folder
        /// </summary>
        /// <returns>
        /// Sanitized relative path
        /// </returns>
        /// <param name='rawPath'>
        /// Raw path, like C:/MyProjects/MyGame/Unity/Assets/Subfolder
        /// Path needs to contain a folder called "Assets"
        /// </param>
        static public string MakeUnityPath(string rawPath, bool includeAssetsFolder = true)
        {
            // Split the path and reassemble into something Unity can read
            string[] splitPath = rawPath.Split(new char[] { '\\', '/' });
            string searchPath = null;

            bool reassemblePath = false;

            for (int i = 0; i < splitPath.Length; i++)
            {
                string name = splitPath[i];

                // Assets Folder found, tells us we should start reassembly of string from here
                if (name == "Assets")
                    reassemblePath = true;

                if (reassemblePath)
                {
                    // Skip Assets folder if defined
                    if (name == "Assets" && !includeAssetsFolder)
                    {
                        // do nothing
                    }
                    // Proceed as normal
                    else
                        searchPath += splitPath[i] + "/";
                }
            }

            // Remove Last Slash     
            if (!string.IsNullOrEmpty(searchPath) && (searchPath.Trim().EndsWith("/") || searchPath.Trim().EndsWith(@"\")))
            {
                //Debug.Log("Last slash found!");
                //searchPath = searchPath.Remove((searchPath.Length - 1), 1);
                searchPath = searchPath.TrimEnd(searchPath[searchPath.Length - 1]);
            }

            return searchPath;
        }

        // Verify a given path for existance
        static public string VerifyPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return "Could not verify Path";

            // Might be a little crude since it does not consider Unity's relative path format
            string getPath = System.IO.Path.GetDirectoryName(path);
            //bool getDirectory = Directory.Exists(path); // can't use this because we deal in unity's relative paths

            if (string.IsNullOrEmpty(getPath))
                return "!! ERROR - PASS FAULTY !!";
            //else if(!getDirectory)
            //return "!! ERROR - DOES NOT EXIST !!";
            else
                return "PATH OK!";
        }

        /// <summary>
        /// Looks through a directory for the file
        /// </summary>
        /// <param name="path"></param>
        /// <param name="extension"></param>
        /// <param name="targetFileName"></param>
        /// <returns>Returns string path if found, else null</returns>
        static public string FindCubemapPath(string path, string extension, string targetFileName)
        {
            string[] files = Directory.GetFiles(path, extension, SearchOption.AllDirectories);

            string newFile = null;

            foreach (string file in files)
            {
                // Get only the Filename and see if this is the file we want
                string filename = Path.GetFileName(file);
                if (filename == targetFileName)
                {
                    //Debug.Log("Match found! " + file);
                    newFile = file;
                }
            }

            return newFile;
        }
    }

    public class CMGenerate : MonoBehaviour
    {
        #region PNG Generate / Texture Functions
        static public void CubemapToPNG(Cubemap sourceCubemap, string outputFolderPath) {
            CubemapToPNG(sourceCubemap, outputFolderPath, true, true, true, true, true, true);
        }

        static public void CubemapToPNG(Cubemap sourceCubemap, string outputFolderPath, bool includePositiveX, bool includeNegativeX, bool includePositiveY, bool includeNegativeY, bool includePositiveZ, bool includeNegativeZ)
        {
            // Set up our local variables for later use
            int sizeX = sourceCubemap.width;
            int sizeY = sourceCubemap.height;
            CubemapFace face = CubemapFace.PositiveX;

            // Loop through all Directions
            for (int b = 0; b < 6; b++)
            {
                switch (b)
                {
                    case 0:
                        if (includePositiveX)
                            face = CubemapFace.PositiveX;
                        break;

                    case 1:
                        if (includeNegativeX)
                            face = CubemapFace.NegativeX;
                        break;

                    case 2:
                        if (includePositiveY)
                            face = CubemapFace.PositiveY;
                        break;

                    case 3:
                        if (includeNegativeY)
                            face = CubemapFace.NegativeY;
                        break;

                    case 4:
                        if (includePositiveZ)
                            face = CubemapFace.PositiveZ;
                        break;

                    case 5:
                        if (includeNegativeZ)
                            face = CubemapFace.NegativeZ;
                        break;
                }

                // If Face is +X but +X is not allowed then we just skip it
                // We have to do this because our initial Face variable is set to PositiveX,
                // which without this condition would cause PositiveX to always be created
                if (face == CubemapFace.PositiveX && !includePositiveX)
                {
                    // Do Nothing
                }
                else
                {
                    //Create the blank texture container
                    Texture2D snapshot = new Texture2D(sizeX, sizeY, TextureFormat.RGB24, true);
                    snapshot.wrapMode = TextureWrapMode.Clamp;

                    Color[] cubemapColors = sourceCubemap.GetPixels(face);
                    snapshot.SetPixels32(CMGenerate.MirrorColor32(cubemapColors)); // Mirror the snapshot image for our PNG in order to be identical with the cubemap faces
                    snapshot.Apply();

                    // Convert to PNG file
                    byte[] bytes = snapshot.EncodeToPNG();

                    // Save the file
                    string path = Application.dataPath + "/" + outputFolderPath + "/" + sourceCubemap.name + " - " + face.ToString() + ".png";
                    System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create);
                    System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs);
                    bw.Write(bytes);
                    bw.Close();
                    fs.Close();

                    // Fix compression state
                    string finalImagePath = CMFile.MakeUnityPath(Application.dataPath + "/" + outputFolderPath + "/" + sourceCubemap.name + " - " + face.ToString() + ".png");
                    if (finalImagePath.Contains("//"))
                        finalImagePath = finalImagePath.Replace("//", "/");

                    AssetDatabase.Refresh(); // refresh necessary before we can use the textureimporter

                    TextureImporter textureImporter = AssetImporter.GetAtPath(finalImagePath) as TextureImporter;
                    if (textureImporter != null)
                    {
                        textureImporter.textureFormat = TextureImporterFormat.RGB24;
                        AssetDatabase.ImportAsset(finalImagePath);
                    }

                    DestroyImmediate(snapshot);
                }
            }
        }

        static public Texture2D Scale(Texture2D source, int targetWidth, int targetHeight)
        {
            Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, true);
            Color32[] rpixels = result.GetPixels32(0);
            float incX = ((float)1 / source.width) * ((float)source.width / targetWidth);
            float incY = ((float)1 / source.height) * ((float)source.height / targetHeight);

            for (int px = 0; px < rpixels.Length; px++)
            {
                rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth), incY * ((float)Mathf.Floor(px / targetWidth)));
            }

            result.SetPixels32(rpixels, 0);
            result.Apply();

            return result;
        }

        static public Color32[] MirrorColor32(Color[] source)
        {
            Color32[] mirroredColors = new Color32[source.Length];

            for (int m = 0; m < source.Length; m++)
            {
                mirroredColors[m] = source[source.Length - 1 - m];
            }

            return mirroredColors;
        }
        #endregion

        static public string GetSceneName(CubemapGenerator.GenerateMethod generateMethod)
        {
            string rString = null;

            if (generateMethod == CubemapGenerator.GenerateMethod.Basic)
            {
                rString = Application.loadedLevelName;
            }
            else if (generateMethod == CubemapGenerator.GenerateMethod.Pro)
            {
                // Get scene name without the path and extension (similar to Path.GetFileNameWithoutExtension() but without the dependency on System.IO namespace)
                //string sceneName = System.IO.Path.GetFileNameWithoutExtension(EditorApplication.currentScene);
                rString = EditorApplication.currentScene;
                rString = rString.Substring(rString.LastIndexOf("/"));
                int sceneExtPos = rString.LastIndexOf(".");
                if (sceneExtPos >= 0)
                    rString = rString.Substring(0, sceneExtPos);
            }
            else
                Debug.LogError("Cubemapper Error: Unknown Scene Name!");

            return rString;
        }


        static public void ToggleNodePreview(bool isActive)
        {
            // Loop through nodes to disable mesh renderer
            CubemapNode[] nodes = FindObjectsOfType(typeof(CubemapNode)) as CubemapNode[];
            for (int i = 0; i < nodes.Length; i++) 
            {
                nodes[i].previewSphere.GetComponent<MeshRenderer>().enabled = isActive;
            }
        }
        
        static public CubemapNode[] GetAllNodes()
        {
            CubemapNode[] nodes;
            nodes = FindObjectsOfType(typeof(CubemapNode)) as CubemapNode[];
            //Debug.Log(nodes.Length + " | " + nodes);
            return (nodes.Length == 0) ? null : nodes;
        }

        static public CubemapUser[] GetAllUsers()
        {
            CubemapUser[] users;
            users = FindObjectsOfType(typeof(CubemapUser)) as CubemapUser[];
            //Debug.Log(users.Length);
            return (users.Length == 0) ? null : users;
        }

        static public void AssignCubemapsToNodes(out bool assigned)
        {
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
            // Undo pre-4.3
		    Undo.RegisterSceneUndo("Cubemap Assigned to Node(s)");
#endif

            CubemapNode[] nodes = CMGenerate.GetAllNodes();

            foreach (CubemapNode node in nodes)
            {
                // Let's find and assign the corresponding Cubemaps if no Cubemap is assigned yet and as long as we explicitly allow assignment
                if (node.cubemap == null || node.allowAssign)
                {
                    // Find Cubemap File
                    string sceneName = Path.GetFileNameWithoutExtension(EditorApplication.currentScene);
                    string filePath = CMFile.FindCubemapPath("Assets/", "*.cubemap", sceneName + " - " + node.name + ".cubemap");
                    string errorNoPathMsg = "Could not find Cubemap for Node: " + node.name + ". Are you sure the file exists?";

                    Cubemap c = AssetDatabase.LoadAssetAtPath(filePath, typeof(Cubemap)) as Cubemap;

                    // Don't proceed if there is no file found at path
                    if (filePath == null || c == null) Debug.LogError(errorNoPathMsg);
                    else
                    {
#if !UNITY_4_0 || !UNITY_4_0_1 || !UNITY_4_1 || !UNITY_4_2
                        // Undo post-4.3
                        Undo.RecordObject(node, "Cubemap Assigned to Node(s)");
#endif
                        node.cubemap = c;
                        Debug.Log("Assigned Cubemap successfully to Node " + node.name);
                    }
                }
                //else Debug.LogWarning(node.name + " already has a Cubemap assigned, skipping assignment. Set Cubemap to None on this Node if this is not what you want.");
            }

            assigned = true;
        }


        static public void AssignCubemapsToUsers()
        {
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
            // Undo pre-4.3
		    Undo.RegisterSceneUndo("Cubemap Assign to Cubemap Users");
#endif
            CubemapNode[] nodes = CMGenerate.GetAllNodes();
            CubemapUser[] users = CMGenerate.GetAllUsers();

            //=======================================//
            // ASSIGN NEAREST NODE'S CUBEMAP TO USER
            //=======================================//
            foreach (CubemapUser user in users)
            {
                CubemapNode nearestNode = null;
                float minDist = Mathf.Infinity;
                Vector3 pos = user.transform.position;

                // Find nearest node
                for (int i = 0; i < nodes.Length; i++)
                {
                    float dist = Vector3.Distance(nodes[i].gameObject.transform.position, pos);

                    if (dist < minDist)
                    {
                        nearestNode = nodes[i];
                        minDist = dist;
                        //Debug.Log("New nearest: " + nearestNode.name + "(Distance: " + nearestDistance + ")");
                    }
                }

                // Assign Cubemap if we can
                if (nearestNode.cubemap == null) Debug.LogError("Trying to access non-existant Cubemap of Node " + nearestNode.name + " by User " + user.name);
                else
                {
                    // Does this user have at least one material somewhere that supports cubemaps?
                    if (VerifyCubemapSupport(user.gameObject))
                    {
                        Renderer[] renderers = user.gameObject.GetComponentsInChildren<Renderer>();

                        if (renderers != null)
                        {
                            List<Material> materialsToEdit = new List<Material>();

                            foreach (Renderer r in renderers)
                            {
                                foreach (Material m in r.sharedMaterials)
                                {
                                    if (m.HasProperty("_Cube"))
                                        materialsToEdit.Add(m);
                                }
                            }

#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
                        // Undo pre-4.3
						Undo.RegisterUndo(materialsToEdit.ToArray(), "Cubemap Assign to Cubemap Users"); 
#endif

                            // Change Materials
                            foreach (Material m in materialsToEdit)
                            {
#if !UNITY_4_0 || !UNITY_4_0_1 || !UNITY_4_1 || !UNITY_4_2
                                // Undo post-4.3
                                Undo.RecordObject(m, "Cubemap Assign to Cubemap Users");
#endif
                                m.SetTexture("_Cube", nearestNode.cubemap);
                            }
                        }

                        //user.renderer.sharedMaterial.SetTexture("_Cube", nearestNode.cubemap);
                        Debug.Log("Assigned Cubemap successfully to \"" + user.name + "\" Shader!");
                    }
                    // no cubemap support anywhere on this object
                    else Debug.LogError(user.gameObject.name + " is a Cubemap User, but no Materials have a _Cube property for attaching Cubemaps!");
                }
            }
        }

        static public void AssignCubemapsToAll()
        {
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2
            // Undo pre-4.3
		    Undo.RegisterSceneUndo("Assigning Cubemaps");
#else
            // Undo post-4.3
            Undo.IncrementCurrentGroup();
#endif
            bool assignedToNodes = false;
            AssignCubemapsToNodes(out assignedToNodes);

            if (assignedToNodes)
                AssignCubemapsToUsers();

#if !UNITY_4_0 || !UNITY_4_0_1 || !UNITY_4_1 || !UNITY_4_2
            // Undo post-4.3 - collapse all previous Undo into one single step
            Undo.CollapseUndoOperations(Undo.GetCurrentGroup());
#endif
        }

        /// <summary>
        /// Verifies that at least one material has the "_Cube" property
        /// </summary>
        /// <returns>
        /// The cubemap support.
        /// </returns>
        /// <param name='obj'>
        /// The object to check
        /// </param>
        static public bool VerifyCubemapSupport(GameObject obj)
        {
            Renderer[] renderers = obj.GetComponentsInChildren<Renderer>();
            bool hasCubemapSupport = false;

            if (renderers != null)
            {
                // Iterate through all renderers
                foreach (Renderer r in renderers)
                {
                    foreach (Material mat in r.sharedMaterials)
                    {
                        if (mat.HasProperty("_Cube")) hasCubemapSupport = true;
                    }
                }
            }
            else hasCubemapSupport = false;

            return hasCubemapSupport;
        }

        static public int CountNodeContainers {
            get
            {
                CubemapNodeContainer[] containers = FindObjectsOfType(typeof(CubemapNodeContainer)) as CubemapNodeContainer[];
                return containers.Length;
            }
        }

        static public int CountNodes
        {
            get
            {
                CubemapNode[] nodes = FindObjectsOfType(typeof(CubemapNode)) as CubemapNode[];
                return nodes.Length;
            }
        }

        static public int CountUsers
        {
            get
            {
                CubemapUser[] users = FindObjectsOfType(typeof(CubemapUser)) as CubemapUser[];
                return users.Length;
            }
        }

        static public bool NodesHaveCubemaps()
        {
            CubemapNode[] nodes = FindObjectsOfType(typeof(CubemapNode)) as CubemapNode[];

            if (nodes.Length == 0) return false;
            else
            {
                // Check our Nodes
                foreach (CubemapNode node in nodes)
                {
                    // Abort and return false if we find one that doesn't have a Cubemap yet
                    if (node.cubemap == null) return false;
                }

                // If we get here, we can assume every node had a cubemap
                return true;
            }
        }
        
        static public bool usingMipMap(Cubemap cubemap)
        {
            SerializedObject obj = new SerializedObject(cubemap);
            if (obj == null) return false;

            SerializedProperty prop = obj.FindProperty("m_MipMap");
            if (prop != null)
                return prop.boolValue;

            return false;
        }

        // Set to use Mip Maps
        static public void SetMipMap(Cubemap cubemap, bool useMipMap)
        {
            SerializedObject obj = new SerializedObject(cubemap);
            if (obj == null) return;

            SerializedProperty prop = obj.FindProperty("m_MipMap");
            if (prop != null)
            {
                prop.boolValue = useMipMap;
                obj.ApplyModifiedProperties();
            }
        }

        // Is this Cubemap in Linear space?
        static public bool isLinear(Cubemap cubemap)
        {
            SerializedObject obj = new SerializedObject(cubemap);
            if (obj == null) return false;

            SerializedProperty prop = obj.FindProperty("m_ColorSpace");
            if (prop != null)
                return prop.intValue == (int)ColorSpace.Gamma;

            return false;
        }

        // Setting of Linear Space
        static public void SetLinear(Cubemap cubemap, bool linear)
        {
            SerializedObject obj = new SerializedObject(cubemap);
            if (obj == null) return;

            SerializedProperty prop = obj.FindProperty("m_ColorSpace");
            if (prop != null)
            {
                prop.intValue = linear ? (int)ColorSpace.Gamma : (int)ColorSpace.Linear;
                obj.ApplyModifiedProperties();
            }
        }
    }

    public class CMEditor
    {
        public static readonly GUIStyle styleSeparator;
        public static readonly GUIStyle styleTableRow;
        public static readonly GUIStyle styleHeadlineTitle;
        public static readonly GUIStyle styleHeadlineDesc;
        public static readonly GUIStyle styleHeadlineIcon;
        public static readonly GUIStyle styleSmallHeadlineTitle;
        public static readonly GUIStyle styleSmallHeadlineDesc;
        public static readonly GUIStyle styleHelpboxOuter;
        public static GUIStyle styleHelpboxInner; // we need to write to it
        public static readonly GUIStyle styleHelpboxText;
        public static readonly GUIStyle styleFoldoutContent;
        public static readonly GUIStyle styleFoldoutTab;

        public static readonly Color colorTableRow1 = Color.gray;
        public static readonly Color colorTableRow2 = new Color(0.8f, 0.8f, 0.8f, 1);

        public static readonly Color ColorBlue = new Color(0.43f, 0.78f, 1f, 1f);
        public static readonly Color ColorGreen = new Color(0.55f, 0.8f, 0.24f, 1f);
        public static readonly Color ColorRed = new Color(0.8f, 0.3f, 0.2f, 1f);
        public static readonly Color ColorOrange = new Color(1f, 0.7f, 0.18f, 1f);

        private static readonly Color separatorColor = EditorGUIUtility.isProSkin ? new Color(0.157f, 0.157f, 0.157f) : new Color(0.5f, 0.5f, 0.5f);

        static CMEditor()
        {
            //GUISkin skin = GUI.skin;

            styleSeparator = new GUIStyle();
            styleSeparator.normal.background = EditorGUIUtility.whiteTexture;
            styleSeparator.stretchWidth = true;
            styleSeparator.margin = new RectOffset(0, 0, 7, 7);

            //styleTableRow = new GUIStyle("InnerShadowBg");
            styleTableRow = new GUIStyle("HelpBox");
            styleTableRow.padding = new RectOffset(4, 4, 4, 4);

            styleHeadlineTitle = new GUIStyle(EditorStyles.label);
            styleHeadlineTitle.fontSize = 18;
            styleHeadlineTitle.padding = new RectOffset(styleHeadlineTitle.padding.left, styleHeadlineTitle.padding.right, styleHeadlineTitle.padding.top, 0);
            styleHeadlineTitle.wordWrap = true;
            //styleHeadlineTitle.normal.textColor = CMEditor.ColorBlue;
            //styleHeadlineTitle.onNormal.textColor = CMEditor.ColorBlue;
            //styleHeadlineTitle.onHover.textColor = CMEditor.ColorBlue;

            styleHeadlineDesc = new GUIStyle(EditorStyles.miniBoldLabel);
            styleHeadlineDesc.fontStyle = FontStyle.Italic;
            styleHeadlineDesc.wordWrap = true;
            styleHeadlineDesc.fontSize = 11;
            styleHeadlineDesc.padding = new RectOffset(styleHeadlineDesc.padding.left, styleHeadlineDesc.padding.right, 0, 15);

            styleHeadlineIcon = new GUIStyle(EditorStyles.label);
            styleHeadlineIcon.fontSize = 40;
            styleHeadlineIcon.padding = new RectOffset(styleHeadlineIcon.padding.left, styleHeadlineIcon.padding.right, -10, 0);
            styleHeadlineIcon.alignment = TextAnchor.UpperCenter;

            styleSmallHeadlineTitle = new GUIStyle(EditorStyles.boldLabel);
            styleSmallHeadlineTitle.fontSize = 13;
            styleSmallHeadlineTitle.padding = new RectOffset(styleSmallHeadlineTitle.padding.left, styleSmallHeadlineTitle.padding.right, styleSmallHeadlineTitle.padding.top, 0);
            styleSmallHeadlineTitle.wordWrap = true;

            styleSmallHeadlineDesc = new GUIStyle(EditorStyles.miniBoldLabel);
            styleSmallHeadlineDesc.fontStyle = FontStyle.Italic;
            styleSmallHeadlineDesc.wordWrap = true;
            styleSmallHeadlineDesc.fontSize = 11;
            styleSmallHeadlineDesc.padding = new RectOffset(styleSmallHeadlineDesc.padding.left, styleSmallHeadlineDesc.padding.right, 0, 5);

            styleHelpboxOuter = new GUIStyle("HelpBox");
            styleHelpboxOuter.padding = new RectOffset(4, 4, 4, 4);
            styleHelpboxInner = new GUIStyle("HelpBox");
            styleHelpboxText = new GUIStyle(EditorStyles.wordWrappedLabel);
            styleHelpboxText.fontSize = 10;

            styleFoldoutContent = new GUIStyle("HelpBox");
            styleFoldoutContent.margin = new RectOffset(styleFoldoutContent.margin.left, styleFoldoutContent.margin.right, 0, styleFoldoutContent.margin.bottom);
            styleFoldoutTab = new GUIStyle(EditorStyles.toolbarDropDown);
            styleFoldoutTab.padding = new RectOffset(10, 0, styleFoldoutTab.padding.top, styleFoldoutTab.padding.bottom);
            styleFoldoutTab.margin = new RectOffset(4, 0, styleFoldoutTab.padding.top, 0);
            styleFoldoutTab.fontSize = 10;
        }

        static public void Headline(string title)
        { Headline(title, null); }
        static public void Headline(string title, string description)
        {
            if (title == null) return;

            GUI.color = CMEditor.ColorBlue;
            GUILayout.Label(title, styleHeadlineTitle);
            GUI.color = Color.white;

            if (description != null)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.BeginVertical();
                    {
                        GUI.color = CMEditor.ColorOrange;
                        GUILayout.Label("\u25CF", styleHeadlineIcon);
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical();
                    {
                        GUI.color = CMEditor.ColorOrange;
                        GUILayout.Label(description, styleHeadlineDesc);
                        GUI.color = Color.white;
                    }
                    EditorGUILayout.EndVertical();                        
                }
                EditorGUILayout.EndHorizontal();
            }
        }

        static public void SmallHeadline(string title)
        { SmallHeadline(title, null); }
        static public void SmallHeadline(string title, string description)
        {
            if (title == null) return;

            GUI.color = CMEditor.ColorBlue;
            GUILayout.Label(title, styleSmallHeadlineTitle);
            GUI.color = Color.white;

            if (description != null)
            {
                GUI.color = CMEditor.ColorOrange;
                GUILayout.Label(description, styleSmallHeadlineDesc);
                GUI.color = Color.white;
            }
        }

        static public void Helpbox(string text, MessageType type)
        {
            int boxOffsetLeft = (type == MessageType.None) ? 8 : 4;
            styleHelpboxInner.padding = new RectOffset(boxOffsetLeft, 8, 8, 8);

            string iconName = null;
            switch(type)
            {
                case MessageType.Info:
                    iconName = "console.infoicon";
                    break;

                case MessageType.Warning:
                    iconName = "console.warnicon";
                    break;

                case MessageType.Error:
                    iconName = "console.erroricon";
                    break;

                case MessageType.None:
                    iconName = null;
                    break;
            }

            EditorGUILayout.BeginHorizontal(styleHelpboxOuter);
            {
                GUI.backgroundColor = Color.gray;
                EditorGUILayout.BeginHorizontal(styleHelpboxInner);
                {
                    if (!string.IsNullOrEmpty(iconName))
                    {
                        Texture2D texture = EditorGUIUtility.FindTexture(iconName);
                        Rect textureRect = GUILayoutUtility.GetRect(texture.width, texture.width, texture.height, texture.height, GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
                        GUI.DrawTexture(textureRect, texture);
                    }

                    EditorGUILayout.LabelField(text, styleHelpboxText);
                }
                EditorGUILayout.EndHorizontal();
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// A foldout where clicking on the text as a whole is enough!
        /// </summary>
        /// <param name="foldout"></param>
        /// <param name="content"></param>
        /// <param name="toggleOnLabelClick"></param>
        /// <param name="style"></param>
        /// <returns></returns>
        public static bool Foldout(bool foldout, GUIContent content, bool toggleOnLabelClick, GUIStyle style)
        {
            Rect position = GUILayoutUtility.GetRect(40f, 40f, 16f, 16f, style);
            // EditorGUI.kNumberW == 40f but is internal
            return EditorGUI.Foldout(position, foldout, content, toggleOnLabelClick, style);
        }
        public static bool Foldout(bool foldout, string content, bool toggleOnLabelClick, GUIStyle style)
        {
            return Foldout(foldout, new GUIContent(content), toggleOnLabelClick, style);
        }

        /// <summary>
        /// A CUSTOM LAYER MASK FIELD (because Unity lacks them for Editor Scripts)
        /// Based on the Code from "Aron Granberg" at Unity Answers
        /// http://answers.unity3d.com/questions/60959/mask-field-in-the-editor.html
        /// </summary>
        /// <param name="label"></param>
        /// <param name="selected"></param>
        /// <param name="showSpecial"></param>
        /// <returns></returns>
        static public LayerMask LayerMaskField(string label, LayerMask selected, bool showSpecial = true)
        {
            List<string> layers = new List<string>();
            List<int> layerNumbers = new List<int>();

            string selectedLayers = "";

            for (int i = 0; i < 32; i++)
            {
                string layerName = LayerMask.LayerToName(i);

                if (layerName != "")
                {
                    if (selected == (selected | (1 << i)))
                    {
                        if (selectedLayers == "")
                            selectedLayers = layerName;
                        else
                            selectedLayers = "Mixed";
                    }
                }
            }

            //EventType lastEvent = Event.current.type; // Only debug

            if (Event.current.type != EventType.MouseDown && Event.current.type != EventType.ExecuteCommand)
            {
                if (selected.value == 0)
                    layers.Add("Nothing");
                else if (selected.value == -1)
                    layers.Add("Everything");
                else
                    layers.Add(selectedLayers);

                layerNumbers.Add(-1);
            }

            if (showSpecial)
            {
                layers.Add((selected.value == 0 ? "[X] " : "     ") + "Nothing");
                layerNumbers.Add(-2);

                layers.Add((selected.value == -1 ? "[X] " : "     ") + "Everything");
                layerNumbers.Add(-3);
            }

            for (int i = 0; i < 32; i++)
            {
                string layerName = LayerMask.LayerToName(i);

                if (layerName != "")
                {
                    if (selected == (selected | (1 << i)))
                        layers.Add("[X] " + layerName);
                    else
                        layers.Add("     " + layerName);

                    layerNumbers.Add(i);
                }
            }

            bool preChange = GUI.changed;

            GUI.changed = false;

            int newSelected = 0;

            if (Event.current.type == EventType.MouseDown)
                newSelected = -1;

            newSelected = EditorGUILayout.Popup(label, newSelected, layers.ToArray(), EditorStyles.layerMaskField);

            if (GUI.changed && newSelected >= 0)
            {
                //newSelected -= 1;
                //Debug.Log (lastEvent +" "+newSelected + " "+layerNumbers[newSelected]);

                if (showSpecial && newSelected == 0)
                    selected = 0;
                else if (showSpecial && newSelected == 1)
                    selected = -1;
                else
                {
                    if (selected == (selected | (1 << layerNumbers[newSelected])))
                    {
                        selected &= ~(1 << layerNumbers[newSelected]);
                        //Debug.Log ("Set Layer "+LayerMask.LayerToName (LayerNumbers[newSelected]) + " To False "+selected.value);
                    }
                    else
                    {
                        //Debug.Log ("Set Layer "+LayerMask.LayerToName (LayerNumbers[newSelected]) + " To True "+selected.value);
                        selected = selected | (1 << layerNumbers[newSelected]);
                    }
                }
            }
            else
            {
                GUI.changed = preChange;
            }

            return selected;
        }


        /// <summary>
        /// Serialized Object Field for Cubemaps, stylizable (but no Labels)
        /// </summary>
        /// <param name="property"></param>
        /// <param name="allowSceneObjects"></param>
        /// <param name="layout"></param>
        static public void SerializedCubemapField(SerializedProperty property, bool allowSceneObjects, GUILayoutOption[] layout)
        {
            Rect position = new Rect(0, 0, 0, 0);
            GUIContent label = new GUIContent();
            label = EditorGUI.BeginProperty(position, label, property);

            EditorGUI.BeginChangeCheck();

            Cubemap newValue = EditorGUILayout.ObjectField(property.objectReferenceValue, typeof(Cubemap), allowSceneObjects, layout) as Cubemap;

            // Only assign the value back if it was actually changed by the user.
            // Otherwise a single value will be assigned to all objects when multi-object editing,
            // even when the user didn't touch the control.
            if (EditorGUI.EndChangeCheck())
                property.objectReferenceValue = newValue;

            EditorGUI.EndProperty();
        }

        /// <summary>
        /// Returns the Clearflags based on a selected number. Useful for custom selection from a Popup List populated by a string array.
        /// It expects the numbers to represent a certain order, 0=Skybox, 1=Solid Color, 2=Depth Only, 3=Don't Clear
        /// </summary>
        /// <param name="selected"></param>
        /// <returns>CameraClearFlags</returns>
        static public CameraClearFlags SetClearFlagFromInt(int selected)
        {
            CameraClearFlags flags = CameraClearFlags.Skybox;

            switch (selected)
            {
                case 0: flags = CameraClearFlags.Skybox; break;
                case 1: flags = CameraClearFlags.SolidColor; break;
                case 2: flags = CameraClearFlags.Depth; break;
                case 3: flags = CameraClearFlags.Nothing; break;
            }

            return flags;
        }

        /********************************************************************
         * SEPERATOR (Graphical) for GUI Elements
         * Based on the free code shared by Lea "numberkruncher" Hayes
         * http://answers.unity3d.com/questions/216584/horizontal-line.html
         ********************************************************************/
        // GUILayout Style
        public static void Separator(Color rgb, float thickness = 2)
        {
            Rect position = GUILayoutUtility.GetRect(GUIContent.none, styleSeparator, GUILayout.Height(thickness));

            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = rgb;
                styleSeparator.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }

        public static void Separator(float thickness, GUIStyle splitterStyle)
        {
            Rect position = GUILayoutUtility.GetRect(GUIContent.none, splitterStyle, GUILayout.Height(thickness));

            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = separatorColor;
                splitterStyle.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }

        public static void Separator(float thickness = 2)
        {
            Separator(thickness, styleSeparator);
        }

        // GUI Style
        public static void Separator(Rect position)
        {
            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = separatorColor;
                styleSeparator.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }
        /*
        // My old way of doing it (meh!)
        static Texture2D seperatorTexture;

        static public void Separator(float padding = 5f)
        {
            GUILayout.Space(padding);

            if (Event.current.type == EventType.Repaint)
            {
                if (seperatorTexture == null)
                    seperatorTexture = MakeTexture();

                Texture2D tex = seperatorTexture;
                Rect rect = GUILayoutUtility.GetLastRect();
                GUI.color = new Color(0f, 0f, 0f, 0.5f);
                GUI.DrawTexture(new Rect(0f, rect.yMin + 6f, Screen.width, 2f), tex);
                GUI.color = Color.white;
            }

            GUILayout.Space(padding);
        }
        static Texture2D MakeTexture()
        {
            Texture2D t = new Texture2D(1, 1);
            t.hideFlags = HideFlags.DontSave;
            t.SetPixel(0, 0, Color.white);
            t.Apply();
            return t;
        }
         */

        /********************************************************************
         * Window Icon Method taken from Open-Source Library HOUnityLibs
         * Link:
         * http://www.holoville.com/games/plugins/hounitylibs/index.php
         * https://code.google.com/p/hounitylibs/source/browse/trunk/HOEditorUtils/HOPanelUtils.cs
        ********************************************************************/
        private static Dictionary<EditorWindow, GUIContent> _winTitleContentByEditor;

        /// <summary>
        /// Sets the icon of an editor window, without changing the title.
        /// </summary>
        /// <param name="editor">Reference to the editor panel whose icon to set</param>
        /// <param name="icon">Icon to apply</param>
        public static void SetWindowTitle(EditorWindow editor, Texture icon)
        { SetWindowTitle(editor, icon, null); }
        /// <summary>
        /// Sets the icon and title of an editor window.
        /// </summary>
        /// <param name="editor">Reference to the editor panel whose icon to set</param>
        /// <param name="icon">Icon to apply</param>
        /// <param name="title">Title</param>
        public static void SetWindowTitle(EditorWindow editor, Texture icon, string title)
        {
            GUIContent titleContent;
            if (_winTitleContentByEditor == null) _winTitleContentByEditor = new Dictionary<EditorWindow, GUIContent>();
            if (_winTitleContentByEditor.ContainsKey(editor))
            {
                titleContent = _winTitleContentByEditor[editor];
                if (titleContent != null)
                {
                    if (titleContent.image != icon) titleContent.image = icon;
                    if (title != null && titleContent.text != title) titleContent.text = title;
                    return;
                }
                _winTitleContentByEditor.Remove(editor);
            }
            titleContent = GetWinTitleContent(editor);
            if (titleContent != null)
            {
                if (titleContent.image != icon) titleContent.image = icon;
                if (title != null && titleContent.text != title) titleContent.text = title;
                _winTitleContentByEditor.Add(editor, titleContent);
            }
        }
        private static GUIContent GetWinTitleContent(EditorWindow editor)
        {
            System.Reflection.PropertyInfo property = typeof(EditorWindow).GetProperty("cachedTitleContent", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            GUIContent result;
            if (property == null)
            {
                result = null;
            }
            else
            {
                result = (property.GetValue(editor, null) as GUIContent);
            }
            return result;
        }
    }
}
#endif