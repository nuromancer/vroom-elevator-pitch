/*------------------------------------------------
// CUBEMAPPER
// Version: See Readme
// Created by: Rainer Liessem
// Website: http://www.spreadcamp.com
//
// PLEASE RESPECT THE LICENSE TERMS THAT YOU
// AGREED UPON WITH YOUR PURCHASE OF THIS ASSET
------------------------------------------------*/
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Cubemapper
{
    [CustomEditor(typeof(CubemapNode)), CanEditMultipleObjects]
    public class CubemapNodeEditor : Editor
    {
        // Overriding Resolution for Cubemaps generated from this Node
        GUIContent[] availableResolutions = new GUIContent[]
	    {
		    new GUIContent("32x32"),
		    new GUIContent("64x64"),
		    new GUIContent("128x128"),
		    new GUIContent("256x256"),
		    new GUIContent("512x512"),
		    new GUIContent("1024x1024"),
		    new GUIContent("2048x2048")
	    };
        int[] resSizes = { 32, 64, 128, 256, 512, 1024, 2048 };

        // Other Serialized Properties
        private SerializedProperty m_allowGeneratePNG;
        private SerializedProperty m_allowCubemapGeneration;
        private SerializedProperty m_allowAssign;
        private SerializedProperty m_overrideResolution;
        private SerializedProperty m_resolution;
        private SerializedProperty m_blendRadius;
        private SerializedProperty m_livePreview;
        private SerializedProperty m_cubemap;

        private void OnEnable()
        {
            m_allowGeneratePNG = serializedObject.FindProperty("allowGeneratePNG");
            m_allowCubemapGeneration = serializedObject.FindProperty("allowCubemapGeneration");
            m_allowAssign = serializedObject.FindProperty("allowAssign");
            m_overrideResolution = serializedObject.FindProperty("overrideResolution");
            m_resolution = serializedObject.FindProperty("resolution");
            m_blendRadius = serializedObject.FindProperty("blendRadius");
            m_livePreview = serializedObject.FindProperty("livePreview");
            m_cubemap = serializedObject.FindProperty("cubemap");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUIUtility.labelWidth = 150f;

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(GUILayout.Width(Screen.width - 30f));
            {
                // Overriding Settings
                GUILayout.Label("Overriding Options", EditorStyles.boldLabel);
                GUILayout.Label("These settings change the generate behavior of this Node. Use with caution!", EditorStyles.wordWrappedMiniLabel);
                EditorGUILayout.PropertyField(m_allowGeneratePNG, new GUIContent("Generate PNGs", "No PNGs can be generated or extracted from this Node if unchecked"));
                EditorGUILayout.PropertyField(m_allowCubemapGeneration, new GUIContent("Generate Cubemaps", "Cubemaps will no longer be generated for this Node if unchecked"));
                EditorGUILayout.PropertyField(m_allowAssign, new GUIContent("Allow Cubemap Assign", "Cubemaps won't be automatically assigned to this Node anymore if unchecked"));
                EditorGUILayout.PropertyField(m_overrideResolution, new GUIContent("Override Resolution?", "If checked, uses a custom resolution for this Node independent of the resolution set in the Cubemapper"));

                if (m_overrideResolution.boolValue)
                    EditorGUILayout.IntPopup(m_resolution, availableResolutions, resSizes, new GUIContent("Resolution", "Custom resolution for this Node independent of the resolution set in the Cubemapper"));

                EditorGUILayout.Space();

                // Live Preview (Pro only)
                GUILayout.Label("Live Preview (Pro only)", EditorStyles.boldLabel);
                if (UnityEditorInternal.InternalEditorUtility.HasPro())
                {
                    EditorGUILayout.PropertyField(m_livePreview, new GUIContent("Enable Live Preview", "Toggle realtime preview instead of showing the assigned Cubemap"));
                    if (m_livePreview.boolValue == true)
                    {
                        GUILayout.Label("NOTE: Live Preview active!\nAssigned Cubemap will not show while active!", EditorStyles.miniBoldLabel);
                    }
                }
                else
                {
                    GUILayout.Label("Disabled - Detected Unity Basic", EditorStyles.miniBoldLabel);
                }

                EditorGUILayout.Space();

                // Stored Cubemap used by this Node
                GUILayout.Label("Assigned Cubemap", EditorStyles.boldLabel);
                CMEditor.SerializedCubemapField(m_cubemap, false, new GUILayoutOption[] { GUILayout.Height(100), GUILayout.Width(100) });
                
                if (m_cubemap.objectReferenceValue != null)
                {
                    EditorGUILayout.BeginHorizontal("HelpBox");
                    EditorGUILayout.LabelField("\"" + m_cubemap.objectReferenceValue.name + "\"", EditorStyles.boldLabel);
                    EditorGUILayout.EndHorizontal();
                }
                else
                {
                    EditorGUILayout.BeginHorizontal("HelpBox");
                    EditorGUILayout.LabelField("None assigned yet", EditorStyles.boldLabel);
                    EditorGUILayout.EndHorizontal();

                    CMEditor.Helpbox("Assign Cubemaps to use \"Cubemap Users\" and their real-time Cubemap switching between Nodes.", MessageType.Info);
                }

                // Blending Settings
                if (m_cubemap.objectReferenceValue != null)
                {
                    EditorGUILayout.Space();

                    GUILayout.Label("Blending Settings", EditorStyles.boldLabel);
                    EditorGUILayout.Slider(m_blendRadius, 0f, Mathf.Infinity, new GUIContent("Blend Radius", "Sphere of influence for Cubemap Users to use if they have Blend Mode set to Radius"));
                    
                    EditorGUILayout.Space();
                }

                // Generate Button for selected Node(s)
                CMEditor.Separator();

                GUI.backgroundColor = CMEditor.ColorGreen;
                if (GUILayout.Button("Generate Cubemap for this Node", GUILayout.Height(40)))
                {
                    if (System.IO.Path.GetFileNameWithoutExtension(EditorApplication.currentScene).Length == 0)
                    {
                        EditorUtility.DisplayDialog("Please save your Scene first!", "You can't use the Cubemapper on unsaved Scenes.", "OK");
                        return;
                    }

                    // Window Set-Up
                    CubemapWindow window = EditorWindow.GetWindow(typeof(CubemapWindow), false, "Cubemapper", true) as CubemapWindow;
                    window.minSize = new Vector2(341, 250);
                    window.autoRepaintOnSceneChange = true;
                    window.Show();

                    // Fill eligible nodes now before generate (there's no way we need to be this tidy about it because of the way multi-object editing is handled, but still)
                    List<CubemapNode> selectedNodes = new List<CubemapNode>();
                    GameObject[] selections = Selection.gameObjects;
                    if (selections.Length > 0)
                    {
                        foreach (GameObject go in selections)
                        {
                            if (go.GetComponent<CubemapNode>())
                                selectedNodes.Add(go.GetComponent<CubemapNode>());
                        }
                    }

                    if (selectedNodes.Count > 0)
                    {
                        window.nodes = selectedNodes.ToArray();
                        window.InitGeneration();
                    }
                    else
                        Debug.LogWarning("Tried to bake individual Node(s), but no active selections were Nodes.");
                }
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.EndVertical();

            // Save Changes
            serializedObject.ApplyModifiedProperties();
        }
    }
}