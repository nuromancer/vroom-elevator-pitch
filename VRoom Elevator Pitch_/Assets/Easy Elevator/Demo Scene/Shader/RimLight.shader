Shader "Custom/RimLight" {
	 Properties {
	 _MainTex ("Base (RGB)", 2D) = "white" {}
     _RimColor ("Rim Color", Color) = (0.26,0.19,0.51,0.0)
     _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
     _Other( "Other", Range(-1.0,2.0) ) = 1.0
     _Other2( "Other2", Range(-5.0,5.0) ) = 1.0
    }
    SubShader { 
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf Lambert
      struct Input {
      	  float2 uv_MainTex;
          float4 color : COLOR;
          float3 viewDir;
      };
      sampler2D _MainTex;
      float4 _RimColor;
      float _RimPower;
      float _Other;
      float _Other2;
      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = 1;
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
          half rim = _Other - saturate(dot (normalize(IN.viewDir)/_Other2, o.Normal));
          o.Emission = _RimColor.rgb * pow (rim, _RimPower);
      }
      ENDCG
    }
    Fallback "Diffuse"
  }
