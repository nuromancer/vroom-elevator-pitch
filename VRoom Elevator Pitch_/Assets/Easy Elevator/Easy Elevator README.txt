﻿Easy Elevator
By: Nifty Studios Inc.
http://www.niftystudios.com 
info@niftystudios.com 


#######################################################################################################
* IMPORTANT: The Resources/LEDPanelTextures folder MUST be placed in your Projects Assets Root folder. 
   * IE: (YourProjectsName)/Assets/Resources/LEDPanelTextures
#######################################################################################################

________________


PACKAGE STRUCTURE:
* IMPORTANT: The Resources/LEDPanelTextures folder MUST be placed in your Projects Assets Root folder. 
   * IE: (YourProjectsName)/Assets/Resources/LEDPanelTextures
* /Easy Elevator/
   * /Demo Scene/
      * /Demo Models/
         * [ DEMO SCENE MODELS ]
         * /Materials/
      * /Demo Prefabs/
         * [ DEMO SCENE PREFABS ]
      * Demo Scene
      * /Demo Scene/
         * [ DEMO SCENE  LIGHTMAPS ]
      * /Demo Script/
         * elevFollow.cs
         * secondCamTrigger.cs
      * /Shader/
         * RimLight.shader
   * /Elevator/
      * /Easy Elevator (non scripted prefabs)/
         * ElevatorPREFAB
         * ElevHallFramePREFAB
      * ElevatorScriptedPREFAB
      * ElevHallFrameGroupPREFAB
      * ElevHallFrameScriptedPREFAB
      * /Models/
         * Elevator
         * /Materials/
      * /Scripts/
         * callBtnTrigger.cs
         * controlTrigger.cs
         * elevControl.cs
         * elevHallFrameController.cs
      * /Textures/
         * [ ELEVATOR TEXTURES ]
* /Resources/
      * /LEDPanelTextures/
         * [ TEXTURES FOR LED DISPLAY ]
* /Standard Assets/


________________


SETUP:
* IMPORTANT: Ensure the Resources/LEDPanelTextures folder is placed in your Projects Assets Root folder. 


1. Drag ElevatorScriptedPREFAB into your scene
   1. Check tag on prefab. If not set, add a tag to the prefab ( ie: elev01 )
   2. In the Inspector window, check that the Hall Frame Tag field in the Elev Control (Script) component has a string assigned ( ie: elev01hallFrame )    
1. Drag ElevHallFrameGroupPREFAB into your scene
   1. Expand the group, select all the Gameobjects inside the group and assure the tag matches the string on the ElevatorScriptedPREFAB > Elev Control (Script) > Hall Frame Tag field
   2. Assure that the string in Elev Tag field of Elev Hall Frame Controller (Script) matches the tag set on the ElevatorScriptedPREFAB


________________


DEFAULT CONTROLS ( found in elevControl.cs > Update() )


HALL:
When close to the call button on the hall frame (inside trigger) , the button highlights to green.
Press E to activate the button (button changes to yellow when pressed)


ELEVATOR:
When close to the elevator panel (inside trigger), the button for the current floor highlights to green.
Press R to highlight the next floor button
Press F to highlight the previous floor button
Press E to activate the button (button changes to yellow)




________________


CLOSER LOOK AT SCRIPTS:


elevControl.cs ( attached to ElevatorScriptedPREFAB )
Button On Mat <Material> >> The material used for the activated buttons
Button Off Mat <Material> >> The material used for the non activated buttons
Button Selector Mat <Material> >> The button highlight material
Led Mat <Material> >> The material used for the both the elevator and hall frame LED displays. 
* IMPORTANT: When using multiple elevators, this material must be unique to each elevator / hall frame combo. This material is assigned to the LED Displays in the Start() function 
Led Panel <Transform> >> The LED Panel of the this elevator
Led Mat Switch Delay <Float> >> The amount of time the LED panel is blank while changing floors
Bth Light Group <Transform> >> The group of button lights on this elevator panel
Hall Frame Tag <String> >> The tag used for the each hall frame gameobject in the HallFrameGoupPREFAB that this elevator can travel to
* IMPORTANT: This string MUST match the tag on the ElevHallFrameScriptedPREFAB gameobjects parented to the ElevHallFrameGroupPREFAB. When using multiple elevators, this string AND tag MUST be different for each elevator / hall frame combo
Cur Floor Level <Int> >> The floor the elevator will be on when the game starts
Time Btwn Floors <Float> >> How long the elevator takes to travel between floors
Doors Open <Bool> >> If true, the doors will be open when the game starts
Wait For Fixed Update <Bool> >> A fix for player jitter when the elevator is moving


elevHallFrameController.cs ( attached to ElevHallFrameScriptedPREFAB )
Floor <Int> >> The floor this hall frame is on
Call button Light <Transform> >> The button light gameobject on this hall frame
Hall Led Panel <Transform> >> The LED display on this hall frame
Elev Tag <String> >> The tag for the elevator associated with this hall frame
* IMPORTANT: This string MUST match the tag on the ElevatorScriptedPREFAB gameobject that will be using this hall frame to travel to. When using multiple elevators, this string AND tag MUST be different for each elevator / hall frame combo


controlTrigger.cs ( attached to ElevatorScriptedPREFAB>TriggerControls  )
( NO CONFIGURATION REQUIRED )


callBtnTrigger.cs ( attached to ElevHallFrameScriptedPREFAB>TriggerCallBtn )
( NO CONFIGURATION REQUIRED )




________________


NOTES:


The position of the ElevatorScriptedPREFAB in the scene is not important. On Start(), the position is set to the Hall Frame position of the current floor set in the ElevatorScriptedPREFAB ( Elev Control ( Script )> Cur Floor Level )


Animations need to be labeled “OpenDoors” / ”CloseDoors”


The textures for the LED mat are, and MUST be in the “[YourUnityProject]/Assets/Resources/LEDPanelTextures” folder. 


If using more than 1 elevator, the LED mat assigned in the inspector for each elevator must be different. The script will assign this material to the elevator LED and the Hall frame LEDs that belong to this elevator.
ie: 3 elevator setup will require 3 LED materials elevLED, elevLED2, elevLED3. Assign each material to 
ElevatorScriptedPREFAB>Elev Control (Script)>Led Mat 
ElevatorScriptedPREFAB2>Elev Control (Script)>Led Mat 
ElevatorScriptedPREFAB3>Elev Control (Script)>Led Mat