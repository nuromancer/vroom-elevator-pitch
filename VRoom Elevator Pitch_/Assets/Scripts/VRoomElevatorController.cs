﻿
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class VRoomElevatorController : MonoBehaviour
{
    public VRoomHallFrameController door;

    public Material buttonOnMat,
                    buttonOffMat,
                    buttonSelectorMat;
    public Material ledMat;
    public Transform ledPanel;
    public float ledMatSwitchDelay = .5f;
    private Material[] ledMatsArray;

    private List<Transform> buttonLightList;
    //private List<GameObject> hallFramesList;
    private List<Texture> texturesList;
    private AnimationClip openAnim,
                                    closeAnim;

    public Transform btnLightGroup;
    public string hallFrameTag = "elev01hallFrame";

    public int curFloorLevel = 0;
    public float timeBtwnFloors = 3;
    public bool doorOpen;
    public bool waitForFixedUpdate = true; //FIXES JITTER WHEN PLAYER IS ON MOVING ELEVATOR
    [HideInInspector]
    public bool useControls,
                useCallBtn,
                isElevMoving;
    [HideInInspector]
    public int prevBtn,
               newFloor;
    private Transform elevator;

    public AudioSource audio;
    public AudioClip openSound;
    public AudioClip closeSound;
    public AudioClip rideSound;
    public AudioClip bingSound;
    public AudioClip selectSound;

    /// <summary>
    /// returns the floor level to move the elevator to. 
    /// A Value of 13 will return the current floor. 
    /// Anything greater than 13 will return one floor lower. (14 is actually 13)
    /// </summary>
    /// <returns>The floor for the elevator to go to.</returns>
    /// <param name="floor">Floor.</param>
    private int floorCheck(int floor)
    {
        if (floor == 13)
            return curFloorLevel;
        if (floor > 12)
            floor -= 1;
        return floor;
    }

  

    void Start()
    {
         //TRANSFORM REFERENCE
        elevator = this.transform;

        //BUTTON LIGHTS OBJECT LIST, POPULATE LIGHTS OBJECT LIST, SORT LIGHTS OBJECT LIST
        buttonLightList = new List<Transform>();
        foreach (Transform btnLight in btnLightGroup)
        {
            buttonLightList.Add(btnLight);
        }
        buttonLightList = buttonLightList.OrderBy(Transform => Transform.name).ToList();


        /*
        //HALL FRAMES OBJECT LIST, POPULATE FRAMES OBJECT LIST, SORT FRAMES OBJECT LIST
        hallFramesList = new List<GameObject>();
        foreach (GameObject hallFrame in GameObject.FindGameObjectsWithTag(hallFrameTag))
        {
            hallFramesList.Add(hallFrame);
        }
        hallFramesList = hallFramesList.OrderBy(GameObject => GameObject.GetComponent<elevHallFrameController>().floor).ToList();

         */
        //LED PANEL TEXTURES LIST, POPULATE TEXTURES LIST, SORT THE LIST
        texturesList = new List<Texture>();
        foreach (Texture tex in Resources.LoadAll("LEDPanelTextures"))
        {
            texturesList.Add(tex);
        }
        texturesList = texturesList.OrderBy(Texture => Texture.name).ToList();

        //SET ANIMATION CLIPS
        openAnim = door.gameObject.transform.animation.GetClip("OpenDoors");
        closeAnim = door.gameObject.transform.animation.GetClip("CloseDoors");

        //ASSIGN LED MATERIALS TO ELEVATOR AND HALL FRAMES, THEN SET LED FLOOR DISPLAY & ELEVATOR TO CURRENT FLOOR
        ledMatsArray = new Material[2];
        ledMatsArray[0] = ledPanel.renderer.material;
        ledMatsArray[1] = ledMat;
        ledPanel.renderer.materials = ledMatsArray;

        /*
        foreach (var hallFrame in hallFramesList)
        {
            hallFrame.GetComponent<elevHallFrameController>().HallLedPanel.renderer.materials = ledMatsArray;
        }
         */ 
        LEDPanel(curFloorLevel);

        //elevator.position = hallFramesList[curFloorLevel].transform.position;

        //SET DOOR OPEN/CLOSE
        if (doorOpen)
        {
            elevator.animation.clip = openAnim;
            elevator.animation[openAnim.name].time = openAnim.length;
            elevator.animation.Play();
            /*
            if (hallFramesList[curFloorLevel].GetComponent<elevHallFrameController>() != null)
            {
                hallFramesList[curFloorLevel].GetComponent<elevHallFrameController>().animation.clip = openAnim;
                hallFramesList[curFloorLevel].GetComponent<elevHallFrameController>().animation[openAnim.name].time = openAnim.length;
                hallFramesList[curFloorLevel].GetComponent<elevHallFrameController>().animation.Play();
            }
             */
        }
        //StartCoroutine(ElevatorTestRoutine());
    }

    IEnumerator ElevatorTestRoutine()
    {
        Debug.Log("Testsequence started.");
        
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(Ride(5, 2.0f));
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Ride(10, 5.0f));
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Ride(15, 5.0f));
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Ride(19, 5.0f));
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Ride(15, 5.0f));
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Ride(10, 5.0f));
        yield return new WaitForSeconds(1.0f);
        //door.CloseDoor();
    }


    IEnumerator Ride(int destinationFloor, float duration)
    {
        Debug.Log("Ride to " + destinationFloor + "th floor.");
        //ButtonFloorLight(destinationFloor, true);        
 
        AudioSource.PlayClipAtPoint(selectSound, audio.transform.position);
            
        yield return new WaitForSeconds(1.0f);

        if (curFloorLevel != destinationFloor)
        {
            audio.Play(); // play elevator sound
            bool rideUp = false;
            int floorsToRide = 0;
            float rideDurationPerFloor = 0.0f;

            floorsToRide = Mathf.Abs(curFloorLevel - destinationFloor);

            if (curFloorLevel < destinationFloor)
            {
                rideUp = true;
            }

            if (rideUp && destinationFloor > 13 && curFloorLevel < 13)
            {
                floorsToRide -= 1;
            }
            else if (!rideUp && destinationFloor < 13 && curFloorLevel > 13)
            {
                floorsToRide -= 1;
            }

            rideDurationPerFloor = duration / floorsToRide;

            while (destinationFloor != curFloorLevel)
            {
                yield return new WaitForSeconds(rideDurationPerFloor);                          

                if (rideUp)
                {

                    ButtonSelect(curFloorLevel + 1);  
                }
                else
                {
                    ButtonSelect(curFloorLevel - 1);
                    
                }
                LEDPanel(curFloorLevel);
                
            }
            audio.Stop(); // stop elevator sound
            AudioSource.PlayClipAtPoint(bingSound, audio.transform.position);
            Debug.Log("Arrived to " + curFloorLevel+ "th floor.");
            // play arrival bing sound
        }
        //Debug.Log("Select " + curFloorLevel + "th floor.");
        //ButtonSelect(curFloorLevel);
        yield return null;
    }

    public void RideToFloor(int destinationFloor, float duration){
        StartCoroutine(Ride(destinationFloor, duration));
    }
    /// <summary>
    /// Buttons the select.
    /// </summary>
    /// <param name="buttonNum">Button number.</param>
    public void ButtonSelect(int buttonNum)
    {
        Debug.Log("Try to select " + buttonNum + "th floor.");
        if (buttonNum == 13 && prevBtn == 12)
        {
            buttonNum = 14;
        }
        if (buttonNum == 13 && prevBtn == 14)
        {
            buttonNum = 12;
        }
        if (buttonNum > 21)
        {
            buttonNum = 0;
        }
        if (buttonNum < 0)
        {
            buttonNum = 21;
        }
        if (buttonNum > 12)
            buttonNum -= 1;

        var selectedBtn = buttonLightList[buttonNum];
        var oldMat = selectedBtn.renderer.material;

        buttonLightList[prevBtn].renderer.material = oldMat;
        selectedBtn.renderer.material = buttonSelectorMat;
        prevBtn = buttonNum;

        curFloorLevel = buttonNum;
        Debug.Log("Button select -> new floor:" + curFloorLevel);
    }


    /// <summary>
    /// Turn Button light ON/OFF for Floor Buttons.
    /// NOTE: USE FLOOR NUMBERS, 0 = Basement!
    /// </summary>
    /// <param name="buttonNum">Button number (THE FLOOR NUMBER).</param>
    /// <param name="turnOn">If set to <c>true</c> turn on.</param>
    void ButtonFloorLight(int buttonNum, bool turnOn)
    {
        //SAFETY CHECK!
        //if (buttonNum < 0 || buttonNum > hallFramesList.Count)
        if (buttonNum < 0 || buttonNum > 19)
            return;

        //CHANGE BUTTON OBJECT MATERIAL
        if (turnOn)
            buttonLightList[buttonNum].renderer.material = buttonOnMat;
        else
            buttonLightList[buttonNum].renderer.material = buttonOffMat;

    }

    /// <summary>
    /// Help Button light ON/OFF.
    /// </summary>
    void ButtonHelpLight(bool turnOn)
    {
        if (turnOn)
            buttonLightList[19].renderer.material = buttonOnMat;
        else
            buttonLightList[19].renderer.material = buttonOffMat; ;
    }


    /// <summary>
    /// Open Button light ON/OFF.
    /// </summary>
    void ButtonOpenLight(bool turnOn)
    {
        if (turnOn)
            buttonLightList[20].renderer.material = buttonOnMat;
        else
            buttonLightList[20].renderer.material = buttonOffMat;
    }


    /// <summary>
    /// Switch LED display to this number
    /// NOTE: USE FLOOR NUMBER
    /// </summary>
    /// <param name="newFlooNum">New Floor number.</param>
    void LEDPanel(int newFloorNum)
    {
        //SAFETY CHECK!
        if (newFloorNum > 19 || newFloorNum < 0)
            return;

        StartCoroutine(LEDPanelSwitch(newFloorNum));
    }
    /// <summary>
    /// Switch LED display from current floor to new floor incrementally
    /// </summary>
    /// <param name="newFloorNum">New floor number.</param>
    /// <param name="floorTime">Time between floors.</param>
    void LEDPanel(int newFloorNum, float floorTime)
    {
        //SAFETY CHECK!
        if (newFloorNum > 19 || newFloorNum < 0)
            return;

        StartCoroutine(LEDPanelSwitch(newFloorNum, floorTime));
    }

    /// <summary>
    /// Switch LED display to this number.
    /// </summary>
    /// <param name="newFloorNum">Floor number.</param>
    IEnumerator LEDPanelSwitch(int newFloorNum)
    {
        //SWITCH TO BLANK LED TEXTRES
        ledMat.SetTexture("_MainTex", texturesList[texturesList.Count - 2]);
        ledMat.SetTexture("_Illum", texturesList[texturesList.Count - 1]);
        //yield return new WaitForSeconds(ledMatSwitchDelay);

        //CONVERT FLOOR NUMBER TO LIST INDEXES
        int illume = (newFloorNum * 2) + 1;
        int diff = illume - 1;

        //CHANGE LED MATERIAL TEXTURES
        ledMat.SetTexture("_MainTex", texturesList[diff]);
        ledMat.SetTexture("_Illum", texturesList[illume]);

        //TURN BUTTON LIGHT OFF
        //ButtonFloorLight(newFloorNum, false);

        //curFloorLevel = newFloorNum;
        yield return null;
    }

    /// <summary>
    /// Switch LED display from current floor to new floor incrementally 
    /// </summary>
    /// <param name="newfloorNum">New Floor number.</param>
    /// <param name="floorTime">Time between Floors.</param>
    IEnumerator LEDPanelSwitch(int newFloorNum, float floorTime)
    {
        //UP OR DOWN
        int floorIncrement = (newFloorNum < curFloorLevel) ? -1 : 1;

        while (curFloorLevel != newFloorNum)
        {
            //SWITCH TO BLANK LED TEXTRES
            ledMat.SetTexture("_MainTex", texturesList[texturesList.Count - 2]);
            ledMat.SetTexture("_Illum", texturesList[texturesList.Count - 1]);
            yield return new WaitForSeconds(ledMatSwitchDelay);

            //CONVERT FLOOR NUMBER TO LIST INDEXES
            int illume = ((curFloorLevel + floorIncrement) * 2) + floorIncrement;
            if (floorIncrement < 0)
                illume = illume + 2;
            int diff = illume - 1;

            //CHANGE LED MATERIAL TEXTURES
            ledMat.SetTexture("_MainTex", texturesList[diff]);
            ledMat.SetTexture("_Illum", texturesList[illume]);
            yield return new WaitForSeconds(floorTime - ledMatSwitchDelay);

            //curFloorLevel += floorIncrement;
        }
        //TURN BUTTON LIGHT OFF
        //ButtonFloorLight(newFloorNum, false);
    }

    /// <summary>
    /// Opens the elevator door only.
    /// </summary>
    public void OpenDoor()
    {
        //transform.animation.clip = openAnim;
        //transform.animation.Play();

        door.gameObject.transform.animation.clip = openAnim;
        door.gameObject.transform.animation.Play();
        Debug.Log("OpenDoor played.");
        AudioSource.PlayClipAtPoint(openSound, audio.transform.position);
        ButtonOpenLight(true);
        doorOpen = true;
    }
    /// <summary>
    /// Opens the elevator & hall door.
    /// </summary>
    /// <param name="floor">Floor.</param>
    public void OpenDoor(int floor)
    {
        transform.animation.clip = openAnim;
        transform.animation.Play();
        
        //hallFramesList[floor].GetComponent<elevHallFrameController>().OpenDoor();
        doorOpen = true;
    }

    /// <summary>
    /// Closes the elevator door only.
    /// </summary>
    public void CloseDoor()
    {
        door.gameObject.transform.animation.clip = closeAnim;
        door.gameObject.transform.animation.Play();
        AudioSource.PlayClipAtPoint(closeSound, audio.transform.position);
        ButtonOpenLight(false);
        doorOpen = false;
    }
    /// <summary>
    /// Closes the elevator & hall door.
    /// </summary>
    /// <param name="floor">Floor.</param>
    public void CloseDoor(int floor)
    {
        transform.animation.clip = closeAnim;
        transform.animation.Play();
        //hallFramesList[floor].GetComponent<elevHallFrameController>().CloseDoor();
        doorOpen = false;
    }       

}


