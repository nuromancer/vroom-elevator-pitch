﻿using UnityEngine;
using System.Collections;

public class MoveForwardScript : MonoBehaviour {

    public float speed;

    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float currentMovementMagnitude = Time.deltaTime * speed + transform.localPosition.z;
        Vector3 newLocalPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, currentMovementMagnitude);
        transform.localPosition = newLocalPosition;
	}
}
