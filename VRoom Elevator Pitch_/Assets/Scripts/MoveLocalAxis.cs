﻿using UnityEngine;
using System.Collections;

public class MoveLocalAxis : MonoBehaviour
{

    public Vector3 axis;
    public float speed;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float currentMovementMagnitude = Time.deltaTime * speed;
        Vector3 newLocalPosition = axis * currentMovementMagnitude + transform.localPosition;
        transform.localPosition = newLocalPosition;
    }
}
