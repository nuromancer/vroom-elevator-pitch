﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class DirectorScript : MonoBehaviour {

	public float PlayerCameraSpeed;
    public float PlayerMoveOutDistance;
    public float ElevatorDoorDuration;

    public GameObject elevator;
    public GearVRMoviePlayer moviePlayer;
    public GameObject cameraController;
    public AudioSource audio;
    
	public List<GameObject> Levels;
    public List<Material> SkyBoxes;
	public List<float> OutsideStayTimes;
    //public List<MovieTexture> VideoFiles;
    public List<float> InsideStayTimes;
    public List<AudioClip> NewFloorArrivalAudios;
    public AudioClip FinishedAudio;
    public List<Transform> ElevatorTransforms;
    public List<string> MovieLocations;
    public List<int> Floors;
    

    GameObject currentLevel;
    string currentVideoFile;
    float currentOutsideStayTime;
    float currentInsideStayTime;

    VRoomElevatorController elevatorController; 
    //MovieTexture currentMovie;
    
   

	// Use this for initialization
	void Start () {

        elevatorController = elevator.GetComponentInChildren<VRoomElevatorController>();
        if (elevatorController == null)
        {
            Debug.Log("No elevator controller found!!!");
        }

        DeactivateScenes();
        StartCoroutine(Director());          

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator Director()
    {
        for (int i=0; i<Levels.Count;i++){

            //yield return StartCoroutine(ClearConsole());  
            
            yield return StartCoroutine(LoadLevel(i));
            yield return StartCoroutine(MoveViewerOutside());   
            yield return StartCoroutine(PlayVideoSequence(i));
                           
        }
        Debug.Log("Finished.");
        AudioSource.PlayClipAtPoint(FinishedAudio, audio.transform.position);

        yield return new WaitForSeconds(5.0f);
        Application.Quit();
    }

    void DeactivateScenes()
    {
        // de-activate all levels except the currentLevel
        foreach (GameObject level in Levels)
        {
            if (level.activeSelf)
            {
                level.SetActive(false);
            }

        }        
    }
    IEnumerator LoadLevel(int index)
    {
        currentLevel = Levels[index];
        currentOutsideStayTime = OutsideStayTimes[index];
              
        
        currentLevel.SetActive(true);

        RenderSettings.skybox = SkyBoxes[index];        

        Debug.Log("Level: *" + currentLevel.name + "* is active.");

        // move elevator & camera to new position in loaded level
        Transform elevatorTransform = ElevatorTransforms[index];
        elevator.transform.position = elevatorTransform.position;
        elevator.transform.rotation = elevatorTransform.rotation;

        AudioClip arrivalClip = NewFloorArrivalAudios[index];

        Debug.Log("Play arrival clip: " + arrivalClip.name);
        AudioSource.PlayClipAtPoint(NewFloorArrivalAudios[index], audio.transform.position);

        yield return null;
    }
       

    IEnumerator PlayVideoSequence(int index)
    {
        Debug.Log("Play video sequence.");
        DeactivateScenes();

        yield return new WaitForSeconds(1.0f);
        
        string currentMovieLocation = MovieLocations[index];
        

        #if (UNITY_EDITOR)
            currentInsideStayTime = InsideStayTimes[index];  
            Debug.Log("Wait inside for " + currentInsideStayTime + " seconds.");
            elevatorController.RideToFloor(Floors[index], currentInsideStayTime);
            yield return new WaitForSeconds(currentInsideStayTime);
        #elif(UNITY_ANDROID && !UNITY_EDITOR) 
            moviePlayer.LoadFile(currentMovieLocation);
            yield return new WaitForSeconds(0.1f);                   
            
            float movieDuration = moviePlayer.GetCurrentMovieLength();
            moviePlayer.Play();

            elevatorController.RideToFloor(Floors[index], 15.0f);

            yield return new WaitForSeconds(15.0f);
            moviePlayer.Stop();
#endif

            yield return new WaitForSeconds(3.0f);
        // currentMovie.Stop();        
    }

    IEnumerator MoveViewerOutside()
    {
        Debug.Log("Move player outside.");
        // wait a second
        yield return new WaitForSeconds(1.0f);
        // open elevator
        Debug.Log("Open elevator.");
        elevatorController.OpenDoor();
        //LeanTween.moveLocalX(elevator, elevator.transform.localPosition.x + 2f, ElevatorDoorDuration).setEase(LeanTweenType.easeInOutCubic);
        yield return new WaitForSeconds(ElevatorDoorDuration);

        // move camera out
        Debug.Log("Move camera out.");
        // LeanTween.moveZ(Camera.main.gameObject, Camera.main.transform.position.z + PlayerMoveOutDistance, PlayerCameraSpeed).setEase(LeanTweenType.easeInOutCubic);
        LeanTween.moveLocalZ(cameraController, cameraController.transform.localPosition.z + PlayerMoveOutDistance, PlayerCameraSpeed).setEase(LeanTweenType.easeInOutCubic);
        yield return new WaitForSeconds(PlayerCameraSpeed + 0.1f);      

        // wait some time
        Debug.Log("Wait outside for " + currentOutsideStayTime + " seconds.");
        yield return new WaitForSeconds(currentOutsideStayTime + 0.1f);

        // move camera back in
        Debug.Log("Move camera back in.");
        LeanTween.moveLocalZ(cameraController, cameraController.transform.localPosition.z - PlayerMoveOutDistance, PlayerCameraSpeed).setEase(LeanTweenType.easeInOutCubic);
        yield return new WaitForSeconds(PlayerCameraSpeed + 0.1f);

        // close elevator
        Debug.Log("Close elevator.");
        elevatorController.CloseDoor();
        yield return new WaitForSeconds(ElevatorDoorDuration);
        //LeanTween.moveLocalX(elevator, elevator.transform.localPosition.x - 2f, ElevatorDoorDuration).setEase(LeanTweenType.easeInOutCubic);
        yield return new WaitForSeconds(ElevatorDoorDuration + 0.1f);
    }


    IEnumerator ClearConsole()
    {
    // wait until console visible
        while(!Debug.developerConsoleVisible)
        {
            yield return null;
        }
        yield return null; // this is required to wait for an additional frame, without this clearing doesn't work (at least for me)
        Debug.ClearDeveloperConsole();
    }

    
}
 
  

