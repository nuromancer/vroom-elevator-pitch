v1.0
Initial release.
v1.1
Folder structure changed.
Added more controls on the shader.
Added a diffuse demo where you can add the hologram shader as a second pass effect.

HOW TO USE:
Look at demo scene and appropriate material.
Add the material to any object and play with material properties.

KNOWN ISSUES:
None

Support:
For any questions, you can contact me from aubergine2010@gmail.com.

Thanks for buying the package.